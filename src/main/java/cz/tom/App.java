package cz.tom;

import java.util.concurrent.Callable;
import picocli.CommandLine;

/**
 * https://picocli.info/
 *
 * @author Tomáš Buchta
 * @created 04/03/2020
 */
@CommandLine.Command(name = "cmdtest",  mixinStandardHelpOptions = true, description = "Prints the sum of 2 numbers.")
public class App
    implements Callable<Integer>{

    @CommandLine.Parameters(index = "0", description = "First number")
    private Long numberOne;

    @CommandLine.Parameters(index = "1", description = "Second number")
    private Long numberTwo;

    @CommandLine.Option(names = {"-o", "--operator"}, description = "+, -, /, *")
    private String operator = "+";

    public static void main(
            String args[])
    {

        final int exitCode = new CommandLine(new App()).execute(args);
        System.exit(exitCode);

    }

    public Integer call()
        throws Exception
    {

         if(operator.equals("+")) {

             System.out.println("Result of addition is: " + (numberOne + numberTwo));

         } else if(operator.equals("-")) {

            System.out.println("Result of subtraction is: " + (numberOne - numberTwo));

        } else if(operator.equals("*")) {

             System.out.println("Result of multiplication is: " + (numberOne * numberTwo));

         } else if(operator.equals("/")) {

             System.out.println("Result of division is: " + (numberOne / numberTwo));

         } else {

             System.out.println("Your operator " + operator + " is not supported.");
         }

         return 0;
    }

}
